# urlwatch-cron

A simple containerization of [urlwatch](https://github.com/thp/urlwatch), 
scheduled with `cron`.

## Usage

Here's an example:
```
podman run -it --rm \
    -e CRON_TIME="*/15 * * * *" \
    -v your_urlwatch_folder:/root/.urlwatch \
    quay.io/andrej/urlwatch-cron:latest
```

You need to mount your urlwatch folder (which contains `urls.yaml`, 
`urlwatch.yaml`,…) to `/root/.urlwatch`.

You specify the `cron` time expression with the `CRON_TIME` environment 
variable, (it's `*/30 * * * *` by default - *every 30 minutes*).

The source code can be found [here](https://gitlab.com/andrejr/urlwatch-cron).
